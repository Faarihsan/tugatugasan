from django.shortcuts import render, redirect
from .models import AddMakanan
from inven.models import Makanan, BahanMakanan
from myinventory.models import NamaBarang
from .forms import AddTerjual
from django.contrib.auth.decorators import login_required
import json
from django.http import HttpResponse

@login_required(login_url='/accounts/login/')
def sold(request, id=None):
    if request.method == 'GET':
        form = AddTerjual()
        data = AddMakanan.objects.all()
    elif request.method == 'POST':
        nama_id = id
        form = AddTerjual(request.POST)
        data_terjual = AddMakanan.objects.get(pk=nama_id)
        data_makanan_terjual = data_terjual.makanan
        barang = NamaBarang.objects.filter(namaproduk = data_makanan_terjual)
        
        if form.is_valid():
            jmlh = form.cleaned_data['makanan']

            #ngurangin jumlah bahan di myinventory
            for tipe_bahan in barang:
                tipe_bahan.jumlah -= tipe_bahan.namabahan.jumlah * jmlh
                tipe_bahan.save()

            #nambahin data jumlahnya
            data_terjual.jumlah_terjual += jmlh
            data_terjual.save()
        return redirect('terjual:sold')


    return render(request, 'terjual/sold.html', {'form':form, 'bahan':data}) 

def ajax_call(request):
    data = Makanan.objects.all()
    result = [len(data)]
    data = json.dumps(result)
    return HttpResponse(data, content_type="application/json")