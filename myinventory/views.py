from django.shortcuts import render, redirect
from django.http import HttpResponse, JsonResponse
from .forms import Barang
from .models import NamaBarang 
import json
from django.contrib.auth.decorators import login_required

@login_required(login_url='/accounts/login/')
def inventory(request, id=None):
    if request.method == 'GET':
        form = Barang()
        data = NamaBarang.objects.all()
    elif request.method == 'POST':
        form = Barang(request.POST)
        if form.is_valid():
            data = NamaBarang.objects.get(pk=id)
            jumlah_lama = data.jumlah
            jumlah_baru = jumlah_lama + form.cleaned_data['makanan']
            data.jumlah = jumlah_baru
            data.save()
        return redirect('myinventory:inventory')
    return render(request, 'inventory.html', {'form':form, 'bahan':data})

def ajax_call(request):
    data = NamaBarang.objects.all()
    result = [len(data)]
    data = json.dumps(result)
    return HttpResponse(data, content_type="application/json")
