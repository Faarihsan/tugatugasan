from django.db import models
from inven.models import BahanMakanan, Makanan

class NamaBarang(models.Model):
    namaproduk = models.ForeignKey('inven.Makanan', default="", on_delete=models.CASCADE)
    namabahan = models.ForeignKey('inven.BahanMakanan', on_delete=models.CASCADE)
    jumlah = models.IntegerField()

    def __str__(self):
        return self.namaproduk.makanan


  