from django import forms
from .models import NamaBarang

class Barang(forms.Form):
    makanan = forms.IntegerField(label='Jumlah', widget=forms.NumberInput(attrs={
        'class' :  'form-control',
        'type' : 'text',
        'aria-label' : "Default",
        'aria-describedby' :"inputGroup-sizing-default"
    }))
        