from django.urls import path
from . import views

app_name = 'myinventory'

urlpatterns = [
    path('', views.inventory, name='inventory'),
    path('<int:id>/', views.inventory, name='inventory'),
    path('ajax_call/', views.ajax_call),
]