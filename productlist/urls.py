from django.urls import path
from . import views

app_name = 'productlist'

urlpatterns = [
    path('productlist', views.productlist, name='productlist'),
    path('ajax_call/', views.ajax_call),
    ]