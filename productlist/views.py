from django.shortcuts import render
from inven.models import Makanan, BahanMakanan
from myinventory.models import NamaBarang
from terjual.models import AddMakanan
from django.contrib.auth.decorators import login_required
import json
from django.http import HttpResponse

@login_required(login_url='/accounts/login/')
def productlist(request):
	data_terjual = AddMakanan.objects.all()
	jml = []
	for i in data_terjual:
		data_makanan_terjual = i.makanan
		barang = NamaBarang.objects.filter(namaproduk = data_makanan_terjual)
		
		for tipe_bahan in barang :
			temp = []
			temp.append(tipe_bahan.namaproduk.makanan)
			temp.append(tipe_bahan.namabahan.bahan)
			temp.append(tipe_bahan.namabahan.jumlah)
			temp.append(tipe_bahan.jumlah)
			if tipe_bahan.namabahan.jumlah < tipe_bahan.jumlah :
				temp.append("masih banyak")
			elif tipe_bahan.namabahan.jumlah > tipe_bahan.jumlah :
				temp.append("kurang")
			else :
				temp.append("ngepas broo")
			jml.append(temp)
	content = {'jml' : jml,}
	return render(request, 'productlist.html',content)

def ajax_call(request):
    data = NamaBarang.objects.all()
    result = [len(data)]
    data = json.dumps(result)
    return HttpResponse(data, content_type="application/json")
